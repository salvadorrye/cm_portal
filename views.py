from django.shortcuts import render 

# Create your views here.
def index(request, template_name, title):
    return render(request, template_name, {'title': title})

