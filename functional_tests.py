from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        #cls.selenium = webdriver.Chrome('/home/pi/chromedriver/chromedriver')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_index_page(self):
        self.selenium.get(self.live_server_url)
        self.assertIn('Camillus MedHaven Portal', self.selenium.title)

    def test_about_page(self):
        self.selenium.get(f'{self.live_server_url}/about/')
        self.assertIn('About', self.selenium.title)
