from django.urls import path
from . import views

app_name = 'cm_portal'

urlpatterns = [
    path('', views.index, {'template_name': 'cm_portal/index.html', 'title': 'Home'}, name='index'),
    path('about/', views.index, {'template_name': 'cm_portal/about.html', 'title': 'About'}, name='about'),
    path('contact/', views.index, {'template_name': 'cm_portal/contact.html', 'title': 'We look forward to hearing from you!'}, name='contact'),
]

